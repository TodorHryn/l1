package com.todorhryn.L1;

public abstract class Converter {
    private static final double inchesInMeter = 39.37;

    static public double metersToInches(double meters) {
        return meters * inchesInMeter;
    }

    static public double inchesToMeters(double meters) {
        return meters / inchesInMeter;
    }
}
