package com.todorhryn.L1;

import com.todorhryn.L1.Handlers.InchesToMetersHandler;
import com.todorhryn.L1.Handlers.MetersToInchesHandler;
import com.todorhryn.L1.Handlers.RegExpDocumentListener;

import javax.swing.*;

public class Main {
    private JFormattedTextField metersField;
    private JFormattedTextField inchesField;
    private JButton metersToInches;
    private JButton inchesToMeters;
    private JPanel back;
    private JLabel metersErrorLabel;
    private JLabel inchesErrorField;

    public Main() {
        metersToInches.addMouseListener(new InchesToMetersHandler(this));
        inchesToMeters.addMouseListener(new MetersToInchesHandler(this));

        metersField.getDocument().addDocumentListener(new RegExpDocumentListener(metersField, metersToInches, metersErrorLabel, "\\+?\\d*(\\.\\d*)?", "Введите число"));
        inchesField.getDocument().addDocumentListener(new RegExpDocumentListener(inchesField, inchesToMeters, inchesErrorField, "\\+?\\d*(\\.\\d*)?", "Введите число"));
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("Main");
        frame.setContentPane(new Main().back);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    public JFormattedTextField getMetersField() {
        return metersField;
    }

    public JFormattedTextField getInchesField() {
        return inchesField;
    }
}
