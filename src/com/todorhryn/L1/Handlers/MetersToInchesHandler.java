package com.todorhryn.L1.Handlers;

import com.todorhryn.L1.Converter;
import com.todorhryn.L1.Main;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class MetersToInchesHandler extends MouseAdapter {
    private Main mainForm;

    public MetersToInchesHandler(Main mainForm) {
        this.mainForm = mainForm;
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        super.mouseClicked(e);

        try {
            double inches = Double.parseDouble(mainForm.getInchesField().getText());
            mainForm.getMetersField().setText(String.valueOf(Converter.inchesToMeters(inches)));
        }
        catch (Exception ex) {
            System.out.println("Exception: " + ex);
        }
    }
}
