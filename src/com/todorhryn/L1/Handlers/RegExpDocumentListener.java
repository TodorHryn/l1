package com.todorhryn.L1.Handlers;

import javax.print.DocFlavor;
import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

public class RegExpDocumentListener implements DocumentListener {
    private JFormattedTextField textField;
    private JButton button;
    private JLabel errorLabel;
    private String regexp, errorText;

    public RegExpDocumentListener(JFormattedTextField textField, JButton button, JLabel errorLabel, String regexp, String errorText) {
        this.textField = textField;
        this.button = button;
        this.errorLabel = errorLabel;
        this.regexp = regexp;
        this.errorText = errorText;
    }

    @Override
    public void insertUpdate(DocumentEvent documentEvent) {
        check();
    }

    @Override
    public void removeUpdate(DocumentEvent documentEvent) {
        check();
    }

    @Override
    public void changedUpdate(DocumentEvent documentEvent) {
        check();
    }

    public void check() {
        boolean matches = textField.getText().matches(regexp);

        button.setEnabled(matches);
        errorLabel.setText(matches ? " " : errorText);
    }
}
