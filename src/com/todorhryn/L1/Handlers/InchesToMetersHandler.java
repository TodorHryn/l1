package com.todorhryn.L1.Handlers;

import com.todorhryn.L1.Converter;
import com.todorhryn.L1.Main;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class InchesToMetersHandler extends MouseAdapter {
    private Main mainForm;

    public InchesToMetersHandler(Main mainForm) {
        this.mainForm = mainForm;
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        super.mouseClicked(e);

        try {
            double meters = Double.parseDouble(mainForm.getMetersField().getText());
            mainForm.getInchesField().setText(String.valueOf(Converter.metersToInches(meters)));
        }
        catch (Exception ex) {
            System.out.println("Exception: " + ex);
        }
    }
}
